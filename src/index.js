import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';

import { Provider } from "react-redux"
import configureStore from './redux/confStore'

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import GlobalLoading from "./components/GlobalLoading/index"

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App />
        <GlobalLoading/>
        <ToastContainer />
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
