import React, { Component } from 'react';
import styles from "./styles"
import { withStyles } from "@material-ui/styles"
import loaddingIcon from './../../assets/img/loadding.gif';
import PropTypes from "prop-types"
import {  compose } from "redux"
import { connect } from "react-redux"

class GlobalLoading extends Component {
    render() {
        const { classes, showLoading } = this.props;
        let xhtml = null;
        if (showLoading) {
            xhtml = <div className={classes.globalLoading}>
                <img src={loaddingIcon} alt="loadding" className={classes.icon} />
            </div>
        }
        return xhtml;
    }
}
GlobalLoading.propTypes = {
    classes: PropTypes.object,
    showLoading: PropTypes.bool
}
const mapStateToProps = (state) => {
    return {
        showLoading: state.ui.showLoading
    }
}
// const mapDispatchToProps = dispatch => {
//     return {
//         uiAction: bindActionCreators(uiAction, dispatch)
//     }
// }
const withConnect = connect(mapStateToProps, null)
export default compose(
    withStyles(styles),
    withConnect
)(GlobalLoading)
// export default withStyles(styles)(withConnect(GlobalLoading));