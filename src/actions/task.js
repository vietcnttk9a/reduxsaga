import * as taskConstance from "./../constants/task"
export const fetchListTask = () => {
    return {
        type: taskConstance.FETCH_TASK
    }
}
export const fetchListTaskSuccess = data => {
    return {
        type: taskConstance.FETCH_TASK_SUCCESS,
        payload: {
            data
        }
    }
}
export const fetchListTaskFailed = error => {
    return {
        type: taskConstance.FETCH_TASK_FAILED,
        payload: {
            error
        }
    }
}
export const filterTask = keyword => {
    return {
        type: taskConstance.FILTER_TASK,
        keyword
    }
}
export const filterTaskSucess = data => {
    return {
        type: taskConstance.FILTER_TASK_SUCCESS,
        payload: { data: data }

    }
}