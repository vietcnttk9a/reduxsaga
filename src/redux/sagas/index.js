import { fork, take, call, put, delay, takeLatest, select } from "redux-saga/effects"
import * as taskTypes from './../../constants/task';
import { getList } from "./../../Services/api/task"
import { STATUS_CODE } from './../../constants/index';
import { fetchListTaskSuccess, fetchListTaskFailed ,filterTaskSucess} from "./../../actions/task"
import { showLoading, hideLoading } from './../../actions/ui'

function* watchFetchListTaskAction(params) {
    while (true) {
        yield take(taskTypes.FETCH_TASK)
        yield put(showLoading())
        const res = yield call(getList)
        const { status, data } = res;
        if (status === STATUS_CODE.SUCCESS) {
            yield put(fetchListTaskSuccess(data))
        } else {
            yield put(fetchListTaskFailed(data))
        }
        yield delay(1000)
        yield put(hideLoading())
    }
}
function* filterTask(payload) {
    yield delay(500)
    const { keyword } = payload;
    console.log("filterTask", payload)
    const list = yield select(state => state.task.listTask)
    const filterTask = list.filter(task => task.title.toLowerCase().includes(keyword.trim().toLowerCase()))
    yield put(filterTaskSucess(filterTask))
}


function* rootSaga(params) {
    yield fork(watchFetchListTaskAction)

    yield takeLatest(taskTypes.FILTER_TASK, filterTask)
}
export default rootSaga;