import * as uiTyppe from "./../constants/ui"
const initalState = {
    showLoading: false
}
const reducer = (state = initalState, action) => {
    switch (action.type) {
        case uiTyppe.HIDE_LOADING:
            return {
                ...state,
                showLoading: false
            }
        case uiTyppe.SHOW_LOADING:
            return {
                ...state,
                showLoading: true
            }
        default:
            return state
    }
}

export default reducer;