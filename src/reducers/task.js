import * as  taskConstance from "./../constants/task"
const initialState = {
    listTask: []
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case taskConstance.FETCH_TASK:
            return { ...state, listTask: [] }
        case taskConstance.FETCH_TASK_SUCCESS:
            var { data } = action.payload;
            return { ...state, listTask: data }
        case taskConstance.FETCH_TASK_FAILED:
            return { ...state, listTask: [] }
        case taskConstance.FILTER_TASK:
            return { ...state }
        case taskConstance.FILTER_TASK_SUCCESS:
            var { data } = action.payload;
            return { ...state, listTask: data }
        default:
            return state
    }
}
export default reducer;