import axiosServier from "./../axiosService"
import { API_ENDPOINT } from "./../../constants/index"

const url = "tasks";
export const getList = () => {
    return axiosServier.get(`${API_ENDPOINT}/${url}`)
}